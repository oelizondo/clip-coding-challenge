<div align="center">
  <h1>Clip Coding Challenge</h1>
  <br>
</div>

[![CircleCI](https://circleci.com/gh/oelizondo/clip-coding-challenge/tree/master.svg?style=svg&circle-token=cb24ce4608d4c985996224730be330bc884b4783)](https://circleci.com/gh/oelizondo/clip-coding-challenge/tree/master)

[![Netlify Status](https://api.netlify.com/api/v1/badges/7b0a9bdd-6bf9-417f-bdf4-79a39350f2a2/deploy-status)](https://app.netlify.com/sites/clipcodingchallenge/deploys)

## Instructions:

Implement a fuzzy search algorithm for transactions with the following requirements

### Todos:

- [x] Have a web page with one input field where a user can enter text, and a displayed list of transactions

- [x] Display the full list of transactions when the input field is empty

- [x] Update the displayed list of transactions as the user types in the input field (only show transactions that match the fuzzy search criteria)

- [x] Show the transactions in order by date

- [x] Fuzzy search must account for all transaction object properties (amount, date, card_last_four)

- [x] You have to use ReactJS for the design/architecture of the application, and you may not use a library for the actual search algorithm

- [x] You can hardcode the transaction data found below and do not need to fetch it from a server (fetching from a server is a plus, you can use http://myjson.com/)

- [x] Unit tests are encouraged

- [x] Develop a beautiful UI, using styled components is a plus

- [x] Deploy your app on heroku or any other platform

Please use the following list of transactions:

```
{
  "transactions": [
    { "amount": 112.98, "date": "27-01-2018T12:34", "card_last_four": "2544" },
    { "amount": 0.45, "date": "01-12-2017T09:36", "card_last_four": "4434" },
    { "amount": 95.99, "date": "23-11-2017T14:34", "card_last_four": "3011" },
    { "amount": 7774.32, "date": "17-07-2017T03:34", "card_last_four": "6051" },
    { "amount": 1345.98, "date": "22-06-2017T10:33", "card_last_four": "0059" },
    { "amount": 2850.70, "date": "27-01-2018T12:34", "card_last_four": "4444" },
    { "amount": 45.00, "date": "10-02-2018T02:34", "card_last_four": "0110" },
    { "amount": 1.00, "date": "17-02-2018T18:34", "card_last_four": "1669" },
    { "amount": 4.69, "date": "01-02-2018T02:34", "card_last_four": "8488" },
    { "amount": 1111.11, "date": "15-01-2018T21:34", "card_last_four": "9912" },
    { "amount": 453.31, "date": "12-11-2018T08:34", "card_last_four": "4321" },
    { "amount": 12.00, "date": "09-01-2018T22:21", "card_last_four": "7654" },
    { "amount": 5000.00, "date": "24-04-2018T18:00", "card_last_four": "6902" },
    { "amount": 25.50, "date": "21-05-2018T17:30", "card_last_four": "5814" },
    { "amount": 150.00, "date": "01-03-2018T10:24", "card_last_four": "7519" },
    { "amount": 893.41, "date": "09-06-2018T21:34", "card_last_four": "7129" },
    { "amount": 100.00, "date": "17-07-2018T13:56", "card_last_four": "4294" },
    { "amount": 250.00, "date": "19-10-2018T20:43", "card_last_four": "5893" },
  ]
}
```

## Desarrollo

La manera en la que empecé a escribir este challenge fue separando—por prioridades—lo que se necesitaba hacer. Separé por capas las características que debería tener la app:

1. Diseño de componentes y eventos.
2. FuzzySearch con los datos de prueba.
3. FuzzySearch con datos de un lugar externo.
3. Estilos.
4. Deployment.

### Diseño de componentes y eventos.

Para hacer el diseño inicial pensé en cómo se debería ver la UI:

1. Un solo input de texto.
2. Una tabla que despliegue las transacciones.
3. Cada renglón de la tabla tiene sus propiedades.

Después pensé en los eventos que tiene que escuchar la aplicación:

1. Cuando el usuario teclea un evento `onChange` debería dispararse y filtrar las transacciones cargadas.
2. El estado de la aplicación cambia y se debería solo desplegar las transacciones encontradas.

Finalmente acabé con 4 componentes princpales:

1. App: que se encarga de la lógica de la aplicación, así como encasillar el estado.
2. SearchBar: recibe una función (de App) como `prop` que se llama cuando se hace el evento `onChange`.
3. Results: es un componente "dummy", es decir que solo recibe datos y los despliega, no tiene lógica.
4. Result: que es un componente de Results, es un renglón individual en la tabla.

Soy un fan personal de los componentes funcionales. Es decir, aquellos que no manejan estados y son "puros". Por eso decidi que todos los componentes excepto `App` deberían ser solo funciones.

También existe la carpeta de `/services` que exporta la función FuzzySearch que busca en las transacciones lo que sea que vaya tecleando el usuario. La búsqueda se hace sobre los tres campos que son proporcionados `amount, date y card_last_four`. Si enuentra resultados en cualquiera de estos campos entonces dicha transaccion es elegible para ser desplegada y se filtra usando el método `filter`.

```javascript
function search(searchText, elementText) {
  return elementText.toString().indexOf(searchText);
}

function performSearch(transaction, searchText) {
  return Object
    .keys(transaction)
    .map(key => search(searchText, transaction[key]))
    .some(val => val >= 0);
}

export default function FuzzySearch (searchText, transactions) {
  return transactions
          .filter(transaction => performSearch(transaction, searchText))
          .sort(sortByDate);
}
```

Por último la carpeta de `/api` utiliza el API de javascript `fetch` para hacer peticiones de HTTP. Utilizando http://myjson.com/ publiqué al rededor de 200 transacciones. Cuando el componente `App` corre el "lifecycle method" `ComponentDidMount` entonces se mandan a llamar los datos. Una ves que se consiguen se actualiza el estado de la aplicación para desplegar los datos iniciales.

### Estilos

Para esta aplicación decidí usar [Styled Components](https://www.styled-components.com/). Una de las razones es que nunca me había tocado utilizar esa estrategia para estilar y quería intentar algo que no fuera plano CSS. Aunque sé que webpack te permite importar estilos directamente en javascript, creo que la propuesta que ofrecen los "styled components" es muy buena y me pareció una buena práctica implementaron.

Ejemplo:

```javascript
import React from 'react';
import styled from 'styled-components';

const HeaderDiv = styled.div`
height: 110px;
width: 100%;
display: flex;
padding-top: 20px;
justify-content: center;
`;

function Header () {
  return (
    <HeaderDiv>
    <h1>Clip Coding Challenge!</h1>
    </HeaderDiv>
  )
}

export default Header;
```

### Deployment

Finalmente decidí hacer un pipeline de CI/CD. Usé [CircleCI](https://circleci.com) para correr las pruebas automatizadas y [Netlify](https://www.netlify.com) para correr el deployment automático cada vez que se hace push a master.

### Referencias

[Styled Components](https://www.styled-components.com)
[Documentación de create react app](https://create-react-app.dev/docs/running-tests/)
[Teoría de FuzzySearch](https://stackoverflow.com/questions/32337135/fuzzy-search-algorithm-approximate-string-matching-algorithm)
[Documentación de React](https://reactjs.org)
