function generateID() {
  let id = 0;

  return function next() {
    id++;
    return id;
  }
}

const idGenerator = generateID();

export function format(transaction) {
  let date = transaction['date'].split('T');
  date[0] = date[0].split('-').reverse().join('-');
  transaction['formatted_date'] = new Date(date.join('T'));
  transaction['id'] = idGenerator();
  return transaction;
}