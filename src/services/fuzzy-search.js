function search(searchText, elementText) {
  return elementText.toString().indexOf(searchText);
}

function performSearch(transaction, searchText) {
  return Object
    .keys(transaction)
    .map(key => search(searchText, transaction[key]))
    .some(val => val >= 0);
}

function sortByDate(dateA, dateB) {
  return dateA['formatted_date'] - dateB['formatted_date'];
}

export default function FuzzySearch (searchText, transactions) {
  return transactions
          .filter(transaction => performSearch(transaction, searchText))
          .sort(sortByDate);
}
