import React from 'react';
import styled from 'styled-components';

const HeaderDiv = styled.div`
  height: 110px;
  width: 100%;
  display: flex;
  padding-top: 20px;
  justify-content: center;
`;

function Header () {
  return (
    <HeaderDiv>
      <h1>Clip Coding Challenge!</h1>
    </HeaderDiv>
  )
}

export default Header;
