import React from 'react';
import styled from 'styled-components';

const Tr = styled.tr`
  text-align: center;
`

const Td = styled.td`
  width: 33%;
  height: 35px;
`

function Result ({ transaction = {} }) {
  return (
    <Tr>
      <Td>{transaction.amount}</Td>
      <Td>{transaction.date}</Td>
      <Td>{transaction.card_last_four}</Td>
    </Tr>
  )
}

export default Result;
