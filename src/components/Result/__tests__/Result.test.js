import React from 'react';
import ReactDOM from 'react-dom';
import Result from '../Result';

it('renders without crashing', () => {
  const table = document.createElement('tbody');
  ReactDOM.render(<Result />, table);
  ReactDOM.unmountComponentAtNode(table);
});
