import React from 'react';
import Result from '../Result/Result';
import styled from 'styled-components';

const TableHeader = styled.div`
  width: 100%;
  display: flex;
  background: #545454;
  color: #FDE5DA;
  padding: 20px 0;
  text-align: center;
`

const TableHead = styled.div`
  width: 33%;
`

const Table = styled.table`
  width: 100%;
  padding-bottom: 50px;
`

function Results ({ transactions }) {
  return (
    <div>
      <TableHeader>
        <TableHead>AMOUNT</TableHead>
        <TableHead>DATE</TableHead>
        <TableHead>LAST4</TableHead>
      </TableHeader>
      <Table>
        <tbody>
          {transactions.map(transaction => <Result transaction={transaction} key={transaction.id}/>)}
        </tbody>
      </Table>
    </div>
  )
}

export default Results;
