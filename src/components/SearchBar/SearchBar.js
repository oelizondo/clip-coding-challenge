import React from 'react';
import styled from 'styled-components';

const SearchBarWrapper = styled.div`
  width: 100%;
  height: 100px;
  margin: 0 auto;
  display: flex;
  justify-content: center;
  padding: 20px 0;
`

const SearchInput = styled.input`
  padding: 10px 10px 10px 0;
  height: 30px;
  width: 500px;
  background-color: #FDE5DA;
  border: transparent;
  border-bottom: 3px solid #545454;
  outline-width: 0;
  font-size: 25px;
  font-family: 'Inria Serif', sans-serif;
`

function SearchBar ({ handleChange }) {
  return (
    <SearchBarWrapper>
      <SearchInput type="number" onChange={handleChange} placeholder="Buscar transacción" />
    </SearchBarWrapper>
  )
}

export default SearchBar;
