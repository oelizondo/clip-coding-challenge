import React, { Component } from 'react';
import getTransactions from '../../api/api';
import SearchBar from '../SearchBar/SearchBar';
import Results from '../Results/Results';
import Header from '../Header/Header';
import FuzzySearch from '../../services/fuzzy-search';
import { format } from '../../services/format-data';
import styled from 'styled-components';

const AppWrapper = styled.div`
  width: 70vw;
  height: 100vh;
  margin: 0 auto;
`

class App extends Component {
  state = { transactions: [], cache: [] }

  handleChange = (event) => {
    const { value } = event.target;
    let transactions = value === '' ? this.state.cache : FuzzySearch(value, this.state.cache);
    this.setState({ transactions });
  }

  componentDidMount () {
    getTransactions()
      .then(data => data.map(format))
      .then(data => this.setState({ cache: data }))
      .catch(e => this.setState({ cache: [] }));
  }

  transactions () {
    return this.state.transactions.length ?
      this.state.transactions :
      this.state.cache;
  }

  render () {
    return (
      <AppWrapper>
        <Header />
        <SearchBar handleChange={this.handleChange} />
        <Results transactions={this.transactions()} />
      </AppWrapper>
    )
  }
}



export default App;
