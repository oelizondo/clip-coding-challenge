const URL = "https://api.myjson.com/bins/7zj6c";

export default function getTransactions () {
  return new Promise ((resolve, reject) => {
    fetch(URL)
      .then(res => res.json())
      .then(data => resolve(data["transactions"]))
      .catch(e => reject(e));
  });
}
